/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty;

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.chatty.channel.message.MessagePacket;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.multiserv.client.PacketHandler;

/**
 *
 * @author thronecth
 */
class MessageHandler extends PacketHandler<MessagePacket> {

    public MessageHandler() {
        super("messagehandler");
    }

    @Override
    public void run(MessagePacket packet) {
        Message msg = packet.getData();
        Channel channel = Chatty.getController().getChannelController().getChannel(msg.getChannel());
        if (channel != null) {
            List<ChatUser> currentUsers = channel.getCurrentUsers();
            Chatty.getPlugin().log(msg.getMessage());
            for (ChatUser user : currentUsers) {
                try {
                    PlayerID from = PlayerIDHandler.getPlayerID(msg.getFrom());
                    user.receiveMessage(from, msg);
                } catch (PlayerIDHandler.NoIdException ex) {
                }
            }
        }
    }

}
