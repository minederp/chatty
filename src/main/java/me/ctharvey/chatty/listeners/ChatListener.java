/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.listeners;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.ChattyController;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.controllers.censors.CensoredWord;
import me.ctharvey.mdbase.formatters.TextFormatter;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author thronecth
 */
public class ChatListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent e) {
        boolean criticalCensor = false;

        //TODO check if muted
        if (!e.isCancelled()) {
            try {
                PlayerID id = PlayerIDS.get(e.getPlayer());
                ChatUser cu = Chatty.getController().getChatUserFactory().getUser(id);
                String[] split = e.getMessage().split(" ");
                HashMap<Integer, CensoredWord> censoredFails = Chatty.getController().getCensorController().getCensoredFails(split);
                criticalCensor = criticalMessageCensor(censoredFails);
                String message;
                if (!e.getPlayer().hasPermission("Chatty.censor.bypass")) {
                    message = filterMessage(split, censoredFails);
                } else {
                    message = e.getMessage();
                }
                if (criticalCensor && !e.getPlayer().hasPermission("Chatty.censor.bypass")) {
                    String badMessage = e.getMessage();
                    for (CensoredWord cWord : censoredFails.values()) {
                        badMessage = badMessage.replace(cWord.getWord(), ChatColor.ITALIC + "" + ChatColor.RED + cWord.getWord());
                    }
                    sendMsgToMods(cu, censoredFails, badMessage);
                    sendMsgToPlayer(cu, censoredFails, badMessage);
                    e.setCancelled(true);
                    return;
                }
                Channel activeChannel = cu.getActiveChannel();
                if (activeChannel == null) {
                    try {
                        throw new Exception("Active channel is null.");
                    } catch (Exception ex) {
                        cu.setActiveChannel(Chatty.getController().getChannelController().getDefaultChannel());
                        Chatty.getController().getChannelController().getDefaultChannel().add(cu);
//                        Logger.getLogger(ChattyController.class.getName()).log(Level.SEVERE, null, ex);
//                        return;
                    }
                }
                cu.getActiveChannel().sendMessage(cu, new Message(e.getPlayer().getName(), message, cu.getActiveChannel().getName()));
                e.setCancelled(true);
            } catch (SQLException ex) {
                Logger.getLogger(ChattyController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private String filterMessage(String[] split, HashMap<Integer, CensoredWord> censoredFails) {
        for (Entry<Integer, CensoredWord> entry : censoredFails.entrySet()) {
            int index = entry.getKey();
            CensoredWord censoredSet = entry.getValue();
            split[index] = censoredSet.getCorrectedWord();
        }
        String join = TextFormatter.join(split, " ", 0);
        return join;
    }

    private boolean criticalMessageCensor(HashMap<Integer, CensoredWord> censoredFails) {
        for (Entry<Integer, CensoredWord> entry : censoredFails.entrySet()) {
            if (!entry.getValue().getReason().isCorrectable()) {
                return true;
            }
        }
        return false;
    }

    private void sendMsgToMods(ChatUser cu, HashMap<Integer, CensoredWord> censoredFails, String message) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(cu.getName()).append("'s censor infraction: ");
            for (CensoredWord censor : censoredFails.values()) {
                sb.append(censor.getReason().getCensorReason()).append(": ").append(censor.getWord());
            }
            Chatty.getPlugin().broadcast("Chatty.censor.view", ChatColor.RED + cu.getName() + "'s BAD MESSAGE:" + ChatColor.RESET + message);
            Chatty.getPlugin().broadcast("Chatty.censor.view", sb.toString());
        } catch (SQLException ex) {
            Logger.getLogger(ChatListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendMsgToPlayer(ChatUser cu, HashMap<Integer, CensoredWord> censoredFails, String message) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("You are guilty of the follwing infractions: ");
            for (CensoredWord censor : censoredFails.values()) {
                sb.append(censor.getReason().getCensorReason()).append(": ").append(censor.getWord());
            }
            Player player = Bukkit.getPlayer(cu.getPlayerID().getUUID());
            Chatty.getPlugin().sendMsg(ChatColor.RED + "BAD MESSAGE: " + ChatColor.RESET + message, player);
            Chatty.getPlugin().sendMsg(sb.toString(), player);
        } catch (SQLException ex) {
            Logger.getLogger(ChatListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
