/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.listeners;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.chatty.chatuser.ChatUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 *
 * @author thronecth
 */
public class CommandListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        try {
            int messageLength = e.getMessage().split(" ").length;
            String cmd;
            ChatUser cu = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get(e.getPlayer()));
            if (messageLength > 1) {
                cmd = e.getMessage().substring(0, e.getMessage().indexOf(" "));
                cmd = cmd.replace("/", "");
            } else {
                cmd = e.getMessage().replace("/", "");
                Channel channelFromShort = Chatty.getController().getChannelController().getChannelFromShort(cu, cmd);
                if (channelFromShort != null && channelFromShort.playerCanJoin(cu)) {
                    if (channelFromShort.setActive(cu)) {
                        Chatty.getPlugin().sendMsg("You have set " + channelFromShort.getLetterColor() + channelFromShort.getName() + " active.", e.getPlayer());
                        e.setCancelled(true);
                    }
                } else {
                    if (channelFromShort != null) {
                        channelFromShort.removeUser(cu);
                        cu.removeChannel(channelFromShort);
                    }
                }
                return;
            }
            //start message after first space
            String msg = e.getMessage().substring(e.getMessage().indexOf(" "), e.getMessage().length());
            msg = msg.substring(1, msg.length());
            Channel channel = Chatty.getController().getChannelController().getChannelFromShort(cu, cmd);
            if (channel == null) {
                return;
            }
            channel.sendMessage(cu, new Message(e.getPlayer().getName(), msg, channel.getName()));
            e.setCancelled(true);
        } catch (SQLException ex) {
            Logger.getLogger(CommandListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
