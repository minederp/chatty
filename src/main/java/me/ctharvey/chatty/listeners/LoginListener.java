/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.listeners;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.chatuser.ChatUserFactory;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 *
 * @author thronecth
 */
public class LoginListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onLogin(PlayerJoinEvent e) {
        try {
            PlayerID id = PlayerIDS.get(e.getPlayer());
            ChatUser cu = Chatty.getController().getChatUserFactory().getUser(id);
            Chatty.getController().getChatUserFactory().getOnlineUsers().put(id, cu);
            if (!"".equals(cu.getNickName())) {
                e.getPlayer().setDisplayName(cu.getNickName());
            }
            List<Channel> validChannels = new ArrayList();
            for (Iterator<Channel> it = cu.getCurrentChannels().iterator(); it.hasNext();) {
                Channel channel = it.next();
                if (channel != null && channel.playerCanJoin(cu)) {
                    validChannels.add(channel);
//                    channel.add(cu);
                } else {
                    it.remove();
                }
            }
            cu.getCurrentChannels().clear();
            for (Channel channel : validChannels) {
                channel.add(cu);
            }
            if (cu.getActiveChannel() == null) {
                cu.setActiveChannel(Chatty.getController().getChannelController().getDefaultChannel());
            }
            Chatty.getPlugin().sendMsg("Setting channel to last active: " + cu.getActiveChannel().getChatColor() + cu.getActiveChannel().getName(), null);
        } catch (SQLException ex) {
            Logger.getLogger(LoginListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
