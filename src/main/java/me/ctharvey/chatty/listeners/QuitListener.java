/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.listeners;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.chatuser.ChatUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author thronecth
 */
public class QuitListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent e) {
        try {
            ChatUser cu = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get(e.getPlayer()));
            HashMap<String, Channel> channels = Chatty.getController().getChannelController().getChannels();
            for (Map.Entry<String, Channel> entry : channels.entrySet()) {
                entry.getValue().removeUser(cu);
            }
            Chatty.getController().getChatUserFactory().getOnlineUsers().remove(cu.getPlayerID());
        } catch (SQLException ex) {
            Logger.getLogger(QuitListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
