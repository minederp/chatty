    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.chatty.channel.message.MessagePacket;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.commands.BlockCommand;
import me.ctharvey.chatty.commands.ChannelCommand;
import me.ctharvey.chatty.commands.NickOffCommand;
import me.ctharvey.chatty.commands.NicknameCommand;
import me.ctharvey.chatty.commands.RealnameCommand;
import me.ctharvey.chatty.commands.SoundCommand;
import me.ctharvey.chatty.commands.UnBlockCommand;
import me.ctharvey.mdbase.MDDatabasePlugin;
import me.ctharvey.mdbase.chat.LocalSend;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.MultiServerHandler;
import me.ctharvey.pluginbase.MessageSender;
import org.bukkit.ChatColor;

/**
 *
 * @author charles
 */
public class Chatty extends MDDatabasePlugin {

    private static ChattyController controller;
    private static Chatty plugin;
    private static final List<CommandInterface> commands = new ArrayList();

    public Chatty() {
        super("Chatty", ChatColor.DARK_GREEN, "[", "]", new LocalSend());
        commands.add(new ChannelCommand(this));
        commands.add(new NicknameCommand(this));
        commands.add(new RealnameCommand(this));
        commands.add(new SoundCommand(this));
        commands.add(new UnBlockCommand(this));
        commands.add(new BlockCommand(this));
        commands.add(new NickOffCommand(this));
        
    }

    public static ChattyController getController() {
        return controller;
    }

    public static Chatty getPlugin() {
        return plugin;
    }

    @Override
    public void startup() {
        plugin = this;
        controller = new ChattyController();
        MultiServerHandler.registerListener(MessagePacket.class, new MessageHandler());
    }

    @Override
    public void shutdown() {
    }

    @Override
    public List<CommandInterface> getCommands() {
        return commands;
    }

}
