/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.channel;

import me.ctharvey.chatty.chatuser.ChatUser;

/**
 *
 * @author Charles
 */
interface ChannelType {
    
    public boolean add(ChatUser cu);
    
    public boolean setActive(ChatUser cu);
    
    public boolean leave(ChatUser cu);
    
    public boolean playerCanJoin(ChatUser cu);
    
}
