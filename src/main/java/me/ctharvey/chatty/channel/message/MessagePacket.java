/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.channel.message;

import me.ctharvey.multiserv.packet.Packet;
import org.json.simple.JSONObject;

/**
 *
 * @author thronecth
 */
public class MessagePacket extends Packet<Message> {

    public MessagePacket(String sendToServer, String sendFromServer, Message data) {
        super(MessagePacket.class, sendToServer, sendFromServer, PacketType.CHAT, data);
    }

    public MessagePacket() {
    }
    
    @Override
    public Message getData() {
        return super.getStoredData();
    }

    @Override
    public void processData(JSONObject jo2) {
        Message message = new Message();
        message.fromJSONObject(jo2);
        this.setData(message);
    }

}
