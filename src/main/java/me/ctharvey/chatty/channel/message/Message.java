/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.channel.message;

import java.sql.SQLException;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.multiserv.packet.JSONable;
import org.json.simple.JSONObject;

/**
 *
 * @author Charles
 */
public class Message implements JSONable {

    public String from, message, channel;

    public Message() {
    }

    public Message(String from, String message, String channel) {
        this.from = from;
        this.message = message;
        this.channel = channel;
    }


    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        jo.put("from", this.from);
        jo.put("msg", this.message);
        jo.put("channel", this.channel);
        return jo;
    }

    @Override
    public void fromJSONObject(JSONObject jo) {
        this.from = (String) jo.get("from");
        this.message = (String) jo.get("msg");
        this.channel = (String) jo.get("channel");
    }

    @Override
    public void fromString(String string) {
    }

    public String getFrom() {
        return from;
    }

    public String getMessage() {
        return message;
    }

    public String getChannel() {
        return channel;
    }

    public boolean isAPing(ChatUser chatUser) throws SQLException {
        return getMessage().toLowerCase().contains(chatUser.getName().toLowerCase())
                || (chatUser.getNickName() != null && !"".equalsIgnoreCase(chatUser.getNickName())) && getMessage().toLowerCase().contains(chatUser.getNickName())
                && !this.getFrom().toLowerCase().equals(chatUser.getName());
    }

    public void setMessage(String string) {
        this.message = string;
    }
}
