/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.channel;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.ChattyController;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.chatty.channel.message.MessagePacket;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.mdbase.vault.VaultManager;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.onlineusers.OnlineUsers;
import me.ctharvey.onlineusers.userinfo.OnlineUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * This class requires you to extend it and add in the implementation functions
 * of channeltype. This will allow custom channels and criteria for being added
 * to be made. Whenever a player joins every channel is automatically checked to
 * see if they're eligible to join unless they have blocked it themselves. Once
 * the channel is created do ChattyController.addChannel to register the
 * channel.
 *
 * @author Charles
 */
public abstract class Channel implements ChannelType {

    private String name, shortname;
    private transient ChatColor letterColor, chatColor;
    private boolean isDefault, filterActive = false, canLeave = true;
    private transient List<ChatUser> currentUsers = new ArrayList();

    public Channel(String name, String shortcut, ChatColor letterColor, ChatColor chatColor, boolean isDefault) {
        this.name = name;
        this.shortname = shortcut;
        this.letterColor = letterColor;
        this.chatColor = chatColor;
        this.isDefault = isDefault;
    }

    @Override
    public String toString() {
        String string = "Channel: " + name + ", Shortname: " + shortname + ", LetterColor: " + letterColor.name() + ", ChatColor: " + chatColor.name();
        return string;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public String getName() {
        return name;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public void addUser(ChatUser chatUser) {
        if (!currentUsers.contains(chatUser)) {
            currentUsers.add(chatUser);
        }
    }

    public void removeUser(ChatUser chatUser) {
        currentUsers.remove(chatUser);
    }

    public String getShortName() {
        return shortname;
    }

    boolean hasUser(ChatUser cu) {
        return currentUsers.contains(cu);
    }

    public boolean isFilterActive() {
        return filterActive;
    }

    public void setFilterActive(boolean filterActive) {
        this.filterActive = filterActive;
    }

    public void sendMessage(ChatUser chatUser, Message message) throws SQLException {

//        String msg = ChatColor.translateAlternateColorCodes('&', "[" + letterColor + shortname + "&f] " + guildTag + prefix + chatUser.getNickName() + suffix + "&7: " + chatColor) + message;
        Player player = Bukkit.getPlayer(chatUser.getPlayerID().getUUID());
        String prefix = VaultManager.getChatHandler().getPlayerPrefix(player);
        String suffix = VaultManager.getChatHandler().getPlayerSuffix(player);
        Resident resident;
        try {
            resident = TownyUniverse.getDataSource().getResident(chatUser.getName());

            Town town = resident.getTown();
            int num = town.getNumResidents();
            String size = "";
            if (num > 0 && num < 5) {
                size = "▁";
            }
            if (num > 5 && num < 10) {
                size = "▂";
            }
            if (num > 10 && num < 15) {
                size = "▃";
            }
            if (num > 15 && num < 20) {
                size = "▄";
            }
            if (num > 20 && num < 30) {
                size = "▅";
            }
            if (num > 30 && num < 40) {
                size = "▆";
            }
            if (num > 40 && num < 50) {
                size = "▇";
            }
            if (num > 50) {
                size = "█";
            }
            if (resident.isMayor()) {
                prefix = "[&6♛" + town.getName() + size + "&f] " + prefix;
            } else {
                prefix = "[&6" + town.getName() + size + "&f] " + prefix;
            }
            Nation nation = town.getNation();
            if (nation != null) {
                if (resident.isKing()) {
                    prefix = "[&4♔" + nation.getName() + "&f]" + prefix;
                }
            }
        } catch (NotRegisteredException ex) {
        }
        message.setMessage(ChatColor.translateAlternateColorCodes('&', "[" + letterColor + shortname + "&f] " + suffix + prefix + chatUser.getNickName() + "&7: " + chatColor) + message.getMessage());
        Chatty.getPlugin().log(this.getName() + " (" + player.getName() + ") " + message.getMessage());
        for (ChatUser user : currentUsers) {
            user.receiveMessage(chatUser.getPlayerID(), message);
        }
        MultiServeClient.sendPacket(new MessagePacket(null, Bukkit.getServerName(), message));
    }

    public boolean canLeave() {
        return canLeave;
    }

    public void setCanLeave(boolean canLeave) {
        this.canLeave = canLeave;
    }

    @Override
    public boolean leave(ChatUser cu) {
        if (canLeave) {
            removeUser(cu);
            return true;
        }
        return false;
    }

    public List<ChatUser> getCurrentUsers() {
        return currentUsers;
    }

    public ChatColor getLetterColor() {
        return letterColor;
    }

    public void setChatColor(ChatColor chatColor) {
        this.chatColor = chatColor;
    }
}
