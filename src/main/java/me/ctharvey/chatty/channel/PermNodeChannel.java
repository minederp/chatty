/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.channel;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.chatty.chatuser.ChatUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class PermNodeChannel extends Channel {

    private String permNode;

    public PermNodeChannel(String name, String shortcut, String permissionNode, ChatColor letterColor, ChatColor chatColor, boolean isDefault) {
        super(name, shortcut, letterColor, chatColor, isDefault);
        this.permNode = permissionNode;
    }

    public String getPermNode() {
        return permNode;
    }

    @Override
    public boolean add(ChatUser cu) {
        try {
            if (this.isDefault() || cu.hasPermission(permNode)) {
                if (!cu.getBlockedChannels().contains(this)) {
                    this.addUser(cu);
                    return cu.addCurrentChannel(this);
                }
            }
            return false;
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public boolean setActive(ChatUser cu) {
        try {
            if (this.isDefault() || cu.hasPermission(permNode)) {
                this.addUser(cu);
                cu.addCurrentChannel(this);
                cu.setActiveChannel(this);
                return true;
            }
            return false;
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public boolean playerCanJoin(ChatUser cu) {
        try {
            return this.isDefault() || cu.hasPermission(permNode);
        } catch (SQLException ex) {
            return false;
        }
    }

    @Override
    public void sendMessage(ChatUser chatUser, Message message) throws SQLException {
        Player player = Bukkit.getPlayer(chatUser.getPlayerID().getUUID());
        if (player.hasPermission(this.permNode) || this.isDefault()) {
            super.sendMessage(chatUser, message); //To change body of generated methods, choose Tools | Templates.
        } else {
            Chatty.getPlugin().sendMsg("You do not have permission to message " + getLetterColor() + getName() + ChatColor.RESET + ". Sending to default Channel.", player);
            this.removeUser(chatUser);
            chatUser.removeChannel(this);
            chatUser.setActiveChannel(Chatty.getController().getChannelController().getDefaultChannel());
            Chatty.getController().getChannelController().getDefaultChannel().sendMessage(chatUser, message);
        }
    }

}
