/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty;

import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.chatuser.ChatUserFactory;
import me.ctharvey.chatty.controllers.CensorController;
import me.ctharvey.chatty.controllers.ChannelController;
import me.ctharvey.chatty.listeners.ChatListener;
import me.ctharvey.chatty.listeners.CommandListener;
import me.ctharvey.chatty.listeners.LoginListener;
import me.ctharvey.chatty.listeners.QuitListener;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author charles
 */
public class ChattyController implements Listener {

    private final ChannelController channelController;
    private final CensorController censorController;

    private final LoginListener loginListener = new LoginListener();
    private final ChatListener chatListener = new ChatListener();
    private final QuitListener quitListener = new QuitListener();
    private final CommandListener commandListener = new CommandListener();

    private final ChatUserFactory chatUserFactory;

    public ChattyController() {
        this.chatUserFactory = new ChatUserFactory(Chatty.getPlugin().getDbHandler(), ChatUser.class);
        channelController = new ChannelController();
        censorController = new CensorController();
        Plugin plugin = Chatty.getPlugin();
        Bukkit.getPluginManager().registerEvents(loginListener, plugin);
        Bukkit.getPluginManager().registerEvents(chatListener, plugin);
        Bukkit.getPluginManager().registerEvents(quitListener, plugin);
        Bukkit.getPluginManager().registerEvents(commandListener, plugin);

    }

    public ChannelController getChannelController() {
        return channelController;
    }

    public CensorController getCensorController() {
        return censorController;
    }

    public ChatUserFactory getChatUserFactory() {
        return chatUserFactory;
    }

}
