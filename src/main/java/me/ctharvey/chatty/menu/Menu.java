/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.menu;

import com.adamantdreamer.foundation.ui.menu.MenuHandler;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class Menu extends com.adamantdreamer.foundation.ui.menu.Menu {

    int cancelSlot;
    boolean addCancel = true;

    public Menu(String title, MenuHandler handler) {
        super(title, handler);
    }

    public boolean isAddCancel() {
        return addCancel;
    }

    public void setAddCancel(boolean addCancel) {
        this.addCancel = addCancel;
    }

    public void addCancel() {
        List<ItemStack> inventory = getIcons();
        double x = (double) (inventory.size()) / 9D;
        int rows = (int) Math.ceil(x);
        if (inventory.size() == rows * 9) {
            cancelSlot = (rows * 9);
        } else {
            cancelSlot = (rows * 9) - 1;
        }
        this.set(cancelSlot, new ItemStack(Material.ANVIL), "Cancel", new String[]{"Click here to cancel menu actions"});
    }

    public void removeCancel() {
        this.set(cancelSlot, new ItemStack(Material.AIR), "", new String[0]);
    }

    public void set(int slot, ItemStack item, String name) {
        if (item.getType() == Material.AIR) {
            return;
        }
        this.set(slot, item, name, new String[0]);
    }

    public void set(int slot, Material mat, String name) {
        if (mat == Material.AIR) {
            return;
        }
        this.set(slot, new ItemStack(mat), name);
    }

    public void set(int slot, Material mat, String name, String... lore) {
        if (mat == Material.AIR) {
            return;
        }
        this.set(slot, new ItemStack(mat), name, lore);

    }

    @Override
    public void show(Player player) {
        if (addCancel) {
            addCancel();
        }
        super.show(player); //To change body of generated methods, choose Tools | Templates.
    }

}
