/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.menu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public abstract class MenuHandler implements com.adamantdreamer.foundation.ui.menu.MenuHandler {

    protected final CommandSender cs;
    protected Object object;
    protected int id;

    public MenuHandler(CommandSender cs, Object object) {
        this.cs = cs;
        this.object = object;
    }

    @Override
    public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
        id = ice.getRawSlot();
        if (id < 0) {
            return Result.DENY;
        }
        switch (ice.getCurrentItem().getType()) {
            case ANVIL:
                if (ice.getCurrentItem().hasItemMeta() && ice.getCurrentItem().getItemMeta().hasDisplayName()) {
                    switch (ChatColor.stripColor(ice.getCurrentItem().getItemMeta().getDisplayName().toLowerCase())) {
                        case "cancel":
                            ice.getView().close();
                            ice.setCancelled(true);
                            return Result.DENY;
                    }
                }
            case AIR:
                return Result.DENY;
        }
        return Result.DEFAULT;
    }

    @Override
    public Event.Result onDrop(PlayerDropItemEvent pdie, boolean bln) {
        return Result.DENY;
    }

    @Override
    public void onClose(InventoryCloseEvent ice) {
        ice.getView().close();
    }

    public boolean isCompleteButton(ItemStack item) {
        if (item.getType().equals(Material.ANVIL) && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().toLowerCase().equals("complete")) {
            return true;
        }
        return false;
    }

}
