/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.controllers.censors.CensoredWord;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class NicknameCommand extends Command {

    public NicknameCommand(MDBasePlugin plugin) {
        super(plugin, "nickname");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        if (strings.length == 0) {
            getPlugin().sendMsg("You need to provide a name to use.", cs);
            return true;
        }
        if (strings.length == 1 && !"set".equals(strings[0])) {
            String nickName = strings[0];
            if (!(cs instanceof ConsoleCommandSender) && !isCensored(cs, nickName)) {
                try {
                    ChatUser cu = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
                    cu.setNickName(nickName);
                    if (!StringUtils.isAlphanumeric(string) && !cs.hasPermission("Chatty.nickname.symbols")) {
                        getPlugin().sendMsg("You do not have permission to use symbols in your nickname.", cs);
                        return true;
                    }
                    if (string.toLowerCase().contains("&k")) {
                        getPlugin().sendMsg("You cannot use the magic symbol for your nick.", cs);
                        return true;
                    }
                    if (string.contains("&") && !cs.hasPermission("Chatty.nickname.colors")) {
                        getPlugin().sendMsg("You do not have permission to use colors in your message.", cs);
                        return true;
                    }
                    if (nickName.length() > 24) {
                        getPlugin().sendMsg("Choose a shorter nickname.", cs);
                        return true;
                    }
                    getPlugin().sendMsg("You have set your nick to &c" + cu.getNickName() + "&f.  Do /nickoff to clear it.", cs);
                } catch (SQLException ex) {
                    Logger.getLogger(NicknameCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (strings.length == 2 || "set".equals(strings[0])) {
            if (strings.length == 2) {
                getPlugin().sendMsg("Format is /nick set <playername> <nickname>", cs);
                return true;
            }
            if (strings.length == 1) {
                getPlugin().sendMsg("You need to provide a name to use.", cs);
                return true;
            }
            if ((cs.isOp() || cs.hasPermission("Chatty.nickname.other")) && strings.length > 2) {
                try {
                    String playerName = strings[1];
                    String nickName = strings[2];
                    if (!isCensored(cs, nickName)) {
                        if (nickName.length() > 24) {
                            getPlugin().sendMsg("Choose a shorter nickname.", cs);
                            return true;
                        }
                        PlayerID id = PlayerIDHandler.getPlayerID(playerName);
                        ChatUser user = Chatty.getController().getChatUserFactory().getUser(id);
                        user.setNickName(nickName);
                        Player player = Bukkit.getPlayer(id.getUUID());
                        if (player != null) {
                            getPlugin().sendMsg("Your nickname has been changed to &c" + nickName + "&f by &b" + cs.getName() + ".  Do /nickoff to remove it.", player);
                        }
                        getPlugin().sendMsg("You set " + playerName + "'s nick to " + nickName, cs);
                    }
                } catch (PlayerIDHandler.NoIdException ex) {
                    getPlugin().sendMsg("Cannot find player specified.", cs);
                }
            } else {
                getPlugin().sendMsg("You do not have permission to do this.", cs);
            }
        }
        return true;
    }

    private boolean isCensored(CommandSender cs, String nickName) {
        HashMap<Integer, CensoredWord> censoredFails = Chatty.getController().getCensorController().getCensoredFails(nickName.split(" "));
        if (censoredFails.isEmpty()) {
            if (Chatty.getController().getChatUserFactory().nickIsAPlayer(nickName)) {
                getPlugin().sendMsg("You are trying to use a name that is already in use.", cs);
                return true;
            }
            return false;
        }
        getPlugin().sendMsg("The nickname is within the censored list.", cs);
        return true;
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList("nick", "nickname", "n");
    }

}
