/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands.subcommands;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class ListSubCommand extends SubCommand {

    public ListSubCommand(Command command) {
        super(command, "list", SubCommandType.ALL, "Chatty.channel.list");
    }

    private Set<Channel> determineEligibleChannels(CommandSender cs) throws SQLException {
        Set<Channel> allowed = new HashSet<>();
        HashMap<String, Channel> channels = Chatty.getController().getChannelController().getChannels();
        for (Channel channel : channels.values()) {
            if (cs.isOp() || channel.playerCanJoin(Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs)))) {
                allowed.add(channel);
            }
        }
        return allowed;
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        try {
            Set<Channel> determineEligibleChannels = determineEligibleChannels(cs);
            getCommand().getPlugin().sendMsg("You have access to the following channels:", cs);
            StringBuilder sb = new StringBuilder();
            for (Channel channel : determineEligibleChannels) {
                sb.append(channel.getLetterColor()).append("[").append(channel.getShortName())
                        .append("]").append(channel.getName())
                        .append(ChatColor.WHITE).append("(")
                        .append(channel.getCurrentUsers().size())
                        .append(ChatColor.WHITE).append(")" + ", ");
            }
            getCommand().getPlugin().sendMsg(sb.toString(), cs);
        } catch (SQLException ex) {
            Logger.getLogger(ListSubCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
