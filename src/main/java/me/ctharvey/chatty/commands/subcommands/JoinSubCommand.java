/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands.subcommands;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.ChattyController;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.chatuser.ChatUserFactory;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class JoinSubCommand extends SubCommand {

    public JoinSubCommand(Command command) {
        super(command, "join", SubCommandType.PLAYER, "Chatty.channel.join");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        try {
            if (args.length == 0) {
                return;
            }
            Channel channel = Chatty.getController().getChannelController().getChannel(args[0]);
            ChatUser cu = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
            if (channel == null || cu == null) {
                sendChannelNotExist(cs, args[0]);
                return;
            }
            if (!channel.getCurrentUsers().contains(cu)) {
                if (channel.add(cu)) {
                    channel.setActive(cu);
                    sendJoinSuccess(cs, channel);
                } else {
                    sendJoinFail(cs, channel);
                }
            } else {
                sendAlreadyIn(cs, channel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(JoinSubCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendChannelNotExist(CommandSender cs, String string) {
        send(cs, "The channel " + string + " does not exist.");
    }

    private void sendJoinSuccess(CommandSender cs, Channel channel) {
        send(cs, "You have joined " + channel.getLetterColor() + channel.getName());
    }

    private void sendJoinFail(CommandSender cs, Channel channel) {
        send(cs, "You are unable to join " + channel.getLetterColor() + channel.getName());
    }

    private void send(CommandSender cs, String msg) {
        getCommand().getPlugin().sendMsg(msg, cs);
    }

    private void sendAlreadyIn(CommandSender cs, Channel channel) {
        send(cs, "You are already in channel " + channel.getLetterColor() + channel.getName());
    }

}
