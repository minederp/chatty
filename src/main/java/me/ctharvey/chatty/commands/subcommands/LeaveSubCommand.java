/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands.subcommands;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.chatuser.ChatUserFactory;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class LeaveSubCommand extends SubCommand {

    public LeaveSubCommand(Command command) {
        super(command, "leave", SubCommandType.PLAYER, "Chatty.channel.leave");
    }

    private void sendChannelNotExist(CommandSender cs, String string) {
        send(cs, "The channel " + string + " does not exist or you are not currently in it.");
    }

    private void sendLeaveChannel(CommandSender cs, Channel channel) {
        send(cs, "You have left " + channel.getLetterColor() + channel.getName());
    }

    private void send(CommandSender cs, String string) {
        getCommand().getPlugin().sendMsg(string, cs);
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        try {
            if (args.length == 0) {
                return;
            }

            ChatUser cu = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
            Channel channel = cu.getChannel(args[0]);
            if (channel == null) {
                sendChannelNotExist(cs, args[0]);
                return;
            }
            channel.removeUser(cu);
            cu.removeChannel(channel);
            sendLeaveChannel(cs, channel);
        } catch (SQLException ex) {
            Logger.getLogger(LeaveSubCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
