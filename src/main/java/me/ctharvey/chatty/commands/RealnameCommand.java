/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.vault.VaultManager;
import me.ctharvey.onlineusers.OnlineUsers;
import me.ctharvey.onlineusers.userinfo.OnlineUser;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 *
 * @author Charles
 */
public class RealnameCommand extends Command implements TabCompleter {

    public RealnameCommand(MDBasePlugin plugin) {
        super(plugin, "realname");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        if (strings.length > 0) {
            List<ChatUser> users = Chatty.getController().getChatUserFactory().getFromNickName(strings[0]);
            if (users != null && !users.isEmpty()) {
                sendListOfNicks(cs, users);
            } else {
                getPlugin().sendMsg("&7Unable to locate anyone with the nickname: &c" + strings[0], cs);
            }
        } else {
            displayAllNicks(cs);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        List<String> names = new ArrayList();
        for (OnlineUser player : OnlineUsers.getOnline()) {
            ChatUser user = Chatty.getController().getChatUserFactory().getUser(player.getPlayerID());
            if (user.hasNickName() && user.getNickName().length() >= strings[0].length() && user.getNickName().replace("~", "").substring(0, strings[0].length()).equals(strings[0])) {
                names.add(user.getNickName().replace("~", ""));
            }
        }
        return names;
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList("realname", "rn");
    }

    private void displayAllNicks(CommandSender cs) {
        HashMap<PlayerID, ChatUser> onlineUsers = Chatty.getController().getChatUserFactory().getOnlineUsers();
        Chatty.getPlugin().sendMsg("Player => Nick", cs);
        for (ChatUser cu : onlineUsers.values()) {
            try {
                if (!cu.getName().equals(cu.getNickName())) {
                    Chatty.getPlugin().sendMsg(cu.getName() + " => " + cu.getNickName(), cs);
                }
            } catch (SQLException ex) {
                Logger.getLogger(RealnameCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void sendListOfNicks(CommandSender cs, List<ChatUser> users) {
        Chatty.getPlugin().sendMsg("Player => Nick", cs);
        for (ChatUser cu : users) {
            try {
                Chatty.getPlugin().sendMsg(cu.getName() + " => " + cu.getNickName(), cs);
            } catch (SQLException ex) {
                Logger.getLogger(RealnameCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
