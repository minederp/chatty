/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class NickOffCommand extends Command {

    public NickOffCommand(MDBasePlugin plugin) {
        super(plugin, "nickoff");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        if (strings.length == 0 && !(cs instanceof ConsoleCommandSender)) {
            try {
                ChatUser user = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
                user.clearNickName();
                Chatty.getPlugin().sendMsg("Your nick has been cleared.", cs);
            } catch (SQLException ex) {
                Logger.getLogger(NickOffCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (strings.length == 1 && (cs.isOp() || cs.hasPermission("Chatty.nickoff.other"))) {
            try {
                PlayerID playerID = PlayerIDHandler.getPlayerID(strings[0]);
                ChatUser cu = Chatty.getController().getChatUserFactory().getUser(playerID);
                cu.clearNickName();
                Player player = Bukkit.getPlayer(playerID.getUUID());
                if (player != null) {
                    Chatty.getPlugin().sendMsg("Your nick has been cleared by " + cs.getName(), player);
                }
                Chatty.getPlugin().sendMsg("You have cleared" + strings[0] + "'s nickname.", cs);
            } catch (PlayerIDHandler.NoIdException ex) {
                Chatty.getPlugin().sendMsg("Unable to find user with name: " + strings[0], cs);
            }
        }
        return true;
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList("nickoff", "no");
    }

}
