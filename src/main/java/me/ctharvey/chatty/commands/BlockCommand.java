/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.chatuser.ChatUserFactory;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class BlockCommand extends Command {

    public BlockCommand(MDBasePlugin plugin) {
        super(plugin, "ignore");
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList("ignore", "i", "block");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] args) {
        try {
            ChatUser user = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
            if (args.length == 0) {
                Set<PlayerID> blockedUsers = user.getBlockedUsers();
                Chatty.getPlugin().sendMsg("Currently blocked: ", cs);
                for (PlayerID userr : blockedUsers) {
                    Chatty.getPlugin().sendMsg(userr.getName(), cs);
                }
                return false;
            }
            ChatUser cuIgnored = Chatty.getController().getChatUserFactory().getUser(PlayerIDHandler.getPlayerID(args[0]));
            if (cuIgnored == null) {
                Chatty.getPlugin().sendMsg("You have tried to block an non-existant user.", cs);
            } else {
                user.addBlockedPlayer(cuIgnored.getPlayerID());
                Chatty.getPlugin().sendMsg("You have blocked" + cuIgnored.getName() + ".", cs);
            }
        } catch (PlayerIDHandler.NoIdException | SQLException ex) {
            Chatty.getPlugin().sendMsg("You have tried to block an non-existant user.", cs);
        }
        return false;
    }
}
