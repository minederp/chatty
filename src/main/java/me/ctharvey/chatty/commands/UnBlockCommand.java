/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.chatuser.ChatUserFactory;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class UnBlockCommand extends Command {

    public UnBlockCommand(MDBasePlugin plugin) {
        super(plugin, "unblock");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] args) {
        try {
            if (args.length == 0) {
                return false;
            }
            ChatUser cu = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
            ChatUser cuIgnored = Chatty.getController().getChatUserFactory().getUser(PlayerIDHandler.getPlayerID(args[0]));
            if (cuIgnored == null || !cu.getBlockedUsers().contains(cuIgnored.getPlayerID())) {
                getPlugin().sendMsg("Either this user does not exist or you do not have them blocked.", cs);
                return false;
            }
            cu.removeBlockedPlayer(cuIgnored.getPlayerID());
            getPlugin().sendMsg("You unblocked "+cuIgnored.getName(), cs);
            return true;
        } catch (SQLException | PlayerIDHandler.NoIdException ex) {
            Logger.getLogger(UnBlockCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

}
