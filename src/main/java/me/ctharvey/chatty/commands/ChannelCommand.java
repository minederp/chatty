/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import java.util.Arrays;
import java.util.List;
import me.ctharvey.chatty.commands.subcommands.JoinSubCommand;
import me.ctharvey.chatty.commands.subcommands.LeaveSubCommand;
import me.ctharvey.chatty.commands.subcommands.ListSubCommand;
import me.ctharvey.pluginbase.MDBasePlugin; 
/**
 *
 * @author Charles
 */
public class ChannelCommand extends me.ctharvey.mdbase.command.Command {

    public ChannelCommand(MDBasePlugin plugin) {
        super(plugin, "channel");
        this.addSubCommand(new JoinSubCommand(this));
        this.addSubCommand(new LeaveSubCommand(this));
        this.addSubCommand(new ListSubCommand(this));
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList("channel", "ch");
    }
}
