/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.commands;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import com.adamantdreamer.foundation.ui.menu.Menu;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.chatuser.ChatUser;
import me.ctharvey.chatty.menu.MenuHandler;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class SoundCommand extends Command {

    public static Random rand = new Random();

    public SoundCommand(MDBasePlugin plugin) {
        super(plugin, "sound");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        Menu menu = new SoundMenu(cs, 0);
        menu.show((Player) cs);
        return true;
    }

    public class SoundMenu extends Menu {

        public SoundMenu(CommandSender cs, int startingSound) {
            super("Select a sound.", new SoundChooserMenuHandler(cs, startingSound));
            int x = startingSound;
            while (x < 53 + startingSound && startingSound + 53 < Sound.values().length) {
                Sound sound = Sound.values()[x];
                this.add(new ItemStack(Material.NOTE_BLOCK), sound.name(), new String[0]);
                x++;
            }
            this.add(new ItemStack(Material.ACTIVATOR_RAIL), "Next Page", new String[0]);
        }

    }

    public class SoundChooserMenuHandler extends MenuHandler {

        public SoundChooserMenuHandler(CommandSender cs, int startingSound) {
            super(cs, startingSound);
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            try {
                if (super.onClick(ice, bln, bln1) == Result.DENY) {
                    return Result.DENY;
                }
                if (ice.getCurrentItem().getType().equals(Material.ACTIVATOR_RAIL)) {
                    Menu menu = new SoundMenu(cs, (int) this.object + 54);
                    menu.show((Player) cs);
                    ice.getView().close();
                    return Result.DENY;
                }
                switch (ice.getClick()) {
                    case LEFT:
                        ChatUser user = Chatty.getController().getChatUserFactory().getUser(PlayerIDS.get((Player) cs));
                        user.setSound(Sound.values()[ice.getRawSlot() + (int) this.object]);
                        break;
                    case RIGHT:
                        ((Player) cs).getWorld().playSound(((Player) cs).getLocation(), Sound.values()[ice.getRawSlot() + (int) this.object], 2, 1);
                        break;
                }
                return Result.DENY;
            } catch (SQLException ex) {
                return Result.DENY;
            }
        }

    }
}
