package me.ctharvey.chatty.chatuser;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import com.adamantdreamer.quorm.query.PreparedQuery;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryResult;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.quormdata.factories.QuormObjectFactory;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class ChatUserFactory extends QuormObjectFactory<ChatUser> {

    private final HashMap<PlayerID, ChatUser> onlineUsers = new HashMap();
    private Query<ChatUser> query = handler.getDao().read(ChatUser.class).where("playerID = ?");

    public ChatUser getUser(PlayerID id) {
        if (onlineUsers.containsKey(id)) {
            return onlineUsers.get(id);
        }
        try {
            PreparedQuery<ChatUser> prepare = query.prepare();
            prepare.set(0, id.getId());
            List<ChatUser> read = prepare.read().readAll();

            if (read == null || read.isEmpty()) {
                ChatUser chatUser = new ChatUser(id);
                add(chatUser);
                onlineUsers.put(id, chatUser);
                return chatUser;
            } else {
                onlineUsers.put(id, read.get(0));
                return read.get(0);
            }
        } catch (SQLException | QuormUniqueNameObjectFactory.InvalidQuormObjectException ex) {
            Logger.getLogger(ChatUserFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ChatUserFactory(QuormHandler handler, Class clazz) {
        super(handler, clazz);
    }

    @Override
    public Map<Integer, ? extends ChatUser> getIndex() {
        return index;
    }

    public boolean nickIsAPlayer(String nickname) {
        try {
            List<PlayerID> get = PlayerIDS.get(nickname);
            if (get == null || get.isEmpty()) {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChatUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public HashMap<PlayerID, ChatUser> getOnlineUsers() {
        return onlineUsers;
    }

    public List<ChatUser> getFromNickName(String string) {
        List<ChatUser> list = new ArrayList();
        for (ChatUser cu : onlineUsers.values()) {
            if (cu.hasNickName() && cu.getNickName().contains(string)) {
                list.add(cu);
            }
        }
        return list;
    }

}
