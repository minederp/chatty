package me.ctharvey.chatty.chatuser;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Type;
import com.adamantdreamer.quorm.define.Var;
import com.adamantdreamer.quorm.elements.DataType;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.channel.message.Message;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory;
import me.ctharvey.quormdata.structures.QuormObject;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 *
 * @author charles
 */
public final class ChatUser extends QuormObject.QuormUniqueIDObject {

    private int playerID;

    @Var
    @Size(16)
    private String nickname;

    @Type(DataType.TEXT)
    private String lastMessage;

    @Type(DataType.TEXT)
    private String activeChannel;

    private Set<String> blockedChannels = new HashSet();
    private Set<String> currentChannels = new HashSet();

    private Set<Integer> blockedUsers = new HashSet();

    @Type(DataType.TEXT)
    private String notifySound = Sound.NOTE_BASS_GUITAR.name();

    private boolean doNotDisturb;

    public ChatUser() {
    }

    public ChatUser(PlayerID id) {
        this.playerID = id.getId();
        this.activeChannel = Chatty.getController().getChannelController().getDefaultChannel().getName();
        this.currentChannels.add(Chatty.getController().getChannelController().getDefaultChannel().getName());
        Chatty.getController().getChannelController().getDefaultChannel().add(this);
    }

    public Set<PlayerID> getBlockedUsers() {
        Set<PlayerID> ids = new HashSet();
        for (Iterator<Integer> it = blockedUsers.iterator(); it.hasNext();) {
            Integer i = it.next();
            try {
                ids.add(PlayerIDS.get(id));
            } catch (SQLException ex) {
                it.remove();
            }
        }
        return ids;
    }

    public boolean addBlockedPlayer(PlayerID id) {
        boolean add = blockedUsers.add(id.getId());
        update();
        return add;
    }

    public boolean removeBlockedPlayer(PlayerID id) {
        boolean remove = blockedUsers.remove(id.getId());
        update();
        return remove;
    }

    public void setActiveChannel(Channel channel) {
        this.activeChannel = channel.getName();
        update();
    }

    public Channel getActiveChannel() {
        return Chatty.getController().getChannelController().getChannel(activeChannel);
    }

    public void receiveMessage(PlayerID id, Message message) {
        try {
            Player player = Bukkit.getPlayer(getPlayerID().getUUID());
            if (player != null && !blockedUsers.contains(id.getId())) {
                player.sendMessage(message.getMessage());
            }
        } catch (SQLException ex) {
            Chatty.getPlugin().logError("This shouldn't happen really unless you're trying to message a player not online.");
        }
    }

    public Set<Channel> getCurrentChannels() {
        Set<Channel> channels = new HashSet();
        for (String name : currentChannels) {
            channels.add(Chatty.getController().getChannelController().getChannel(name));
        }
        return channels;
    }

    public Set<Channel> getBlockedChannels() {
        Set<Channel> channels = new HashSet();
        for (String channel : blockedChannels) {
            channels.add(Chatty.getController().getChannelController().getChannel(channel));
        }
        return channels;
    }

    public boolean addBlockedChannel(Channel channel) {
        boolean add = blockedChannels.add(channel.getName());
        update();
        return add;
    }

    //todo bug when leaving/join channels having to do with blockign perms?
    public boolean removeBlockedChannel(Channel channel) {
        boolean remove = blockedChannels.remove(channel.getName());
        update();
        return remove;
    }

    public String getNickName() {
        if (nickname == null || nickname.isEmpty()) {
            try {
                return getPlayerID().getName();
            } catch (SQLException ex) {
                Logger.getLogger(ChatUser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return nickname;
    }

//    public void setNickName(String nickname) throws SQLException, Exception {
//        if (!StringUtils.isAlphanumeric(nickname)) {
//            sendMessage("CONSOLE", "You have entered an invalid character for nicknames.");
//            return;
//        }
//        if (nickname.length() > 16) {
//            sendMessage("CONSOLE", "Unable to set nicknamess this long");
//            return;
//        }
//        if (nickname.length() < 3) {
//            sendMessage("CONSOLE", "Unable to set nicknames this short.");
//        }
//        if (nickname.toLowerCase().contains("derp") && me.ctharvey.Helper.Main.permHandler.has("", username, "Friendzoned.nick.derp")) {
//            sendMessage("CONSOLE", "Derp in your nick is reserved for those who earn it.");
//            return;
//        }
//        if (ChatUser.userExists(nickname)) {
//            sendMessage("CONSOLE", "Please do not impersonate anyone on the server.");
//            return;
//        }
//        if (Chatty.getDb().userInfoTable.getNickName(nickname) != null) {
//            sendMessage("CONSOLE", "This nickname is already in use.  No dupes.");
//            return;
//        }
//        if (getBukPlayer() != null) {
//            Chatty.send(getBukPlayer(), "Your nick has changed to " + nickname);
//        }
//        nickname = nickname.replaceAll("[^A-Za-z0-9]", "");
//        ChatUserFactory.setNickName(this, nickname);
//        this.nickname = nickname;
//        if (getBukPlayer() != null) {
//            getBukPlayer().setDisplayName(nickname);
//        }
//    }
    public void clearNickName() {
        try {
            Player player = Bukkit.getPlayer(getPlayerID().getUUID());
            if (player != null) {
                player.setDisplayName(null);
            }
            this.nickname = null;
            update();
        } catch (SQLException ex) {
            Logger.getLogger(ChatUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isDoNotDisturbOn() {
        return this.doNotDisturb;
    }

    public void toggleDoNotDisturb() {
        this.doNotDisturb = !this.doNotDisturb;
        update();
    }

    public String getName() throws SQLException {
        return getPlayerID().getName();
    }

    public boolean addCurrentChannel(Channel channel) {
        boolean add = currentChannels.add(channel.getName());
        update();
        return add;
    }

    public boolean removeChannel(Channel channel) {
        boolean remove = currentChannels.remove(channel.getName());
        update();
        return remove;
    }

    public Channel getChannel(String string) {
        for (Channel channel : getCurrentChannels()) {
            if (channel.getName().toLowerCase().equals(string.toLowerCase())) {
                return channel;
            }
            if (channel.getShortName().toLowerCase().equals(string.toLowerCase())) {
                return channel;
            }
        }
        return null;
    }

    public boolean hasNickName() {
        return nickname != null && !"".equals(nickname);
    }

    public Sound getSound() {
        return Sound.valueOf(this.notifySound);
    }

    public void setSound(Sound sound) {
        this.notifySound = sound.name();
        update();
    }

    @Override
    public Class getClassType() {
        return ChatUser.class;
    }

    @Override
    public boolean update() {
        try {
            Chatty.getController().getChatUserFactory().update(this);
        } catch (SQLException | QuormUniqueNameObjectFactory.InvalidQuormObjectException ex) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete() {
        return false;
    }

    @Override
    public Object clone() {
        return null;
    }

    public PlayerID getPlayerID() throws SQLException {
        return PlayerIDS.get(playerID);
    }

//    public void sendMessage(String userFrom, String message) {
//        if (!ignoredPlayers.contains(userFrom)) {
//            try {
//                if (((message.contains(this.nickname) && !this.nickname.isEmpty()) || message.contains(this.username)) && !userFrom.equals(this.username)) {
//                    if (!this.nickname.isEmpty()) {
//                        message = message.replace(this.nickname, ChatColor.RED + this.nickname);
//                    }
//                    message = message.replace(this.username, ChatColor.RED + this.username);
//                    getBukPlayer().getWorld().playSound(getBukPlayer().getLocation(), sound, 2, 1);
//                }
//                Bukkit.getPlayer(username).sendMessage(message);
//            } catch (NullPointerException e) {
//                Helper.styleLog("&cERROR.  &7ATTEMPTING TO MESSAGE NULL PLAYER: " + username);
//            }
//        }
//    }
    public boolean hasPermission(String permNode) throws SQLException {
        Player player = Bukkit.getPlayer(getPlayerID().getUUID());
        if (player == null) {
            return false;
        }
        return (player.hasPermission(permNode) || player.isOp());
    }

    public void setNickName(String nickName) {
        this.nickname = nickName;
        this.update();
    }

}
