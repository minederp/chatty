/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers;

import java.util.HashMap;
import java.util.Map;
import me.ctharvey.chatty.channel.Channel;
import me.ctharvey.chatty.channel.PermNodeChannel;
import me.ctharvey.chatty.chatuser.ChatUser;
import org.bukkit.ChatColor;

/**
 *
 * @author thronecth
 */
public final class ChannelController {

    private final HashMap<String, Channel> channels = new HashMap();
    private final Channel defaultChannel = new PermNodeChannel("Universal", "u", "", ChatColor.GREEN, ChatColor.WHITE, true);

    public ChannelController() {
        this.addChannel(defaultChannel);
        this.addChannel(new PermNodeChannel("Staff", "st", "chatty.channel.staff", ChatColor.DARK_GREEN, ChatColor.DARK_GREEN, false));
        this.addChannel(new PermNodeChannel("Mod", "m", "chatty.channel.mod", ChatColor.AQUA, ChatColor.AQUA, false));
        this.addChannel(new PermNodeChannel("Admin", "admin", "chatty.channel.admin", ChatColor.RED, ChatColor.RED, false));
    }

    public void addChannel(Channel channel) {
        channels.put(channel.getName(), channel);
    }

    public HashMap<String, Channel> getChannels() {
        return channels;
    }

    public Channel getChannel(String channel) {
        if (channels.containsKey(channel)) {
            return channels.get(channel);
        } else {
            for (Map.Entry<String, Channel> entry : channels.entrySet()) {
                if (entry.getValue().getShortName().equals(channel)) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }

    public Channel getChannelFromShort(ChatUser cu, String shortName) {
        for (Channel channel : cu.getCurrentChannels()) {
            if (channel != null) {
                if (channel.getShortName().equalsIgnoreCase(shortName)) {
                    return channel;
                }
            }
        }
        return null;
    }

    public Channel getDefaultChannel() {
        return this.defaultChannel;
    }

    public void removeChannel(Channel channel) {
        channels.remove(channel.getName());
    }

}
