/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

import net.minecraft.util.org.apache.commons.lang3.StringUtils;

/**
 *
 * @author thronecth
 */
public class CensorReasonCaps extends CensorReason {

    public CensorReasonCaps() {
        correctable = true;
        this.censorReason = "Using caps";
    }

    @Override
    public boolean isCensored(String word) {
        try {
            if (StringUtils.isAllUpperCase(word) && word.length() > 2) {
                this.corrected = word.toLowerCase();
                return true;
            }
        } catch (NullPointerException ex) {
            return false;
        }
        return false;
    }

}
