/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import me.ctharvey.chatty.Chatty;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;

/**
 *
 * @author thronecth
 */
public abstract class CensorReason {

    private Set<String> words = new HashSet();
    protected boolean correctable = false;
    protected String corrected;
    protected String censorReason;

    public final static CensorReason IPADDRESS = new CensorReasonIPAddress();
    public final static CensorReason WEBSITE = new CensorReasonWebsite();
    public final static CensorReason BIGOTRY = new CensorReasonBigotry();
    public final static CensorReason BADWORD = new CensorReasonBadWord();
    public final static CensorReason SPAM = new CensorReasonSpam();
    public final static CensorReason CAPS = new CensorReasonCaps();

    public static CensorReason[] values() {
        List<CensorReason> reasons = new ArrayList();
        reasons.add(IPADDRESS);
        reasons.add(BIGOTRY);
        reasons.add(BADWORD);
        reasons.add(SPAM);
        reasons.add(CAPS);
        return reasons.toArray(new CensorReason[0]);
    }

    public boolean isCensored(String word) {
        for (String aWord : words) {
            if (word.toLowerCase().contains(aWord.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String getConfigSection() {
        return null;
    }

    public void addWord(String string) {
        this.words.add(string);
    }

    public void addWords(List<String> list) {
        if (list != null && !list.isEmpty()) {
            words.addAll(list);
        } else {
            Chatty.getPlugin().log("Unable to add words to censor list for: " + getClass().getSimpleName());
        }
    }

    public String getCorrect() {
        return corrected;
    }

    public boolean isCorrectable() {
        return correctable;
    }

    public String getCensorReason() {
        if (censorReason == null) {
            return "";
        } else {
            return ChatColor.RED + censorReason;
        }
    }

    public Set<String> getWords() {
        return words;
    }

}
