/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

import com.sun.jndi.toolkit.url.Uri;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thronecth
 */
public class CensorReasonWebsite extends CensorReason {

    public CensorReasonWebsite() {
        this.censorReason = "Posting a website";
    }

    @Override
    public boolean isCensored(String word) {
        if (word.contains(".")) {
            try {
                if (word.toLowerCase().contains("minederp") || word.toLowerCase().contains("minecraft")) {
                    return false;
                }
                URL u = new URL(word);
                URI toURI = u.toURI();
                return true;
            } catch (MalformedURLException | URISyntaxException ex) {
                try {
                    URL u = new URL("http://" + word);
                    u.toURI();
                    return true;
                } catch (MalformedURLException | URISyntaxException ex1) {
                    return false;
                }
            }

        }
        return false;
    }

}
