/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

import java.util.regex.Pattern;
import me.ctharvey.chatty.Chatty;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author thronecth
 */
public class CensorReasonIPAddress extends CensorReason {

    public CensorReasonIPAddress() {
        this.censorReason = "IP Address";
    }

    @Override
    public boolean isCensored(String word) {
        if (isIPv4Address(word)) {
            return true;
        }
        if (word.contains(":") && word.length() > 1) {
            String[] split = word.split(":");
            if (isIPv4Address(split[0])) {
                return true;
            }
        }
        return false;
    }

    private static final Pattern IPV4_PATTERN
            = Pattern.compile(
                    "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");

    public static boolean isIPv4Address(String input) {
        input = input.replaceAll("[^\\d.]", "");
        return IPV4_PATTERN.matcher(input).find();
    }

}
