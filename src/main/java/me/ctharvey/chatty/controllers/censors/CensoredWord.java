/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

/**
 *
 * @author thronecth
 */
public class CensoredWord {

    private final String word;
    private final CensorReason reason;
    private final String correctedWord;

    public CensoredWord(String word, CensorReason reason, String correctedWord) {
        this.word = word;
        this.reason = reason;
        this.correctedWord = correctedWord;
    }

    public String getCorrectedWord() {
        return correctedWord;
    }

    public String getWord() {
        return word;
    }

    public CensorReason getReason() {
        return reason;
    }

}
