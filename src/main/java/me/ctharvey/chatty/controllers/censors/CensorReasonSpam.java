/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

/**
 *
 * @author thronecth
 */
public class CensorReasonSpam extends CensorReason {

    public CensorReasonSpam() {
        this.correctable = true;
        this.censorReason = "Spamming";
    }

    @Override
    public boolean isCensored(String word) {
        StringBuilder chars = new StringBuilder();
        boolean censored = false;
        char oldchar = 0;
        int charcnt = 0;
        int repeatchar = 0;
        for (char c : word.toCharArray()) {
            if (charcnt > 1) {
                if (c == oldchar) {
                    repeatchar++;
                    if (repeatchar <= 1 || Character.isDigit(c)) {
                        chars.append(c);
                    } else {
                        censored = true;
                    }
                } else {
                    repeatchar = 0;
                    chars.append(c);
                }
            } else {
                chars.append(c);
            }
            charcnt++;
            oldchar = c;
        }
        corrected = chars.toString();
        return censored;
    }
}
