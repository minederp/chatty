/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers.censors;

/**
 *
 * @author thronecth
 */
public class CensorReasonBadWord extends CensorReason {

    public CensorReasonBadWord() {
        this.censorReason = "Bad word usage";
    }

    @Override
    public String getConfigSection() {
        return "badwords";
    }

    @Override
    public boolean isCensored(String word) {
        for (String aWord : getWords()) {
            if (word.toLowerCase().contains(aWord)) {
                return true;
            }
        }
        return false;
    }

}
