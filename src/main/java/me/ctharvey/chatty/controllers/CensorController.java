/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.chatty.controllers;

import java.util.HashMap;
import java.util.List;
import me.ctharvey.chatty.Chatty;
import me.ctharvey.chatty.controllers.censors.CensorReason;
import me.ctharvey.chatty.controllers.censors.CensoredWord;
import me.ctharvey.dataobjects.yml.ConfigurationNode;

/**
 *
 * @author thronecth
 */
public class CensorController {

    private final HashMap<String, String> censorList = new HashMap<>();
    private final ConfigurationNode mainNode = new ConfigurationNode(Chatty.getPlugin().getConfig().getRoot(), "Censor");

    public CensorController() {
        for (CensorReason reason : CensorReason.values()) {
            String configSection = reason.getConfigSection();
            if (configSection != null) {
                ConfigurationNode reasonNode = new ConfigurationNode(mainNode, reason.getConfigSection());
                List<String> list = (List<String>) reasonNode.get();
                reason.addWords(list);
            }
        }
    }

    /**
     *
     * @param words
     * @return hashamp with word index and censoredword value
     */
    public HashMap<Integer, CensoredWord> getCensoredFails(String[] words) {
        HashMap<Integer, CensoredWord> list = new HashMap();
        int cnt = 0;
        for (String word : words) {
            if (!getWhitelistedWords().contains(word)) {
                for (CensorReason reason : CensorReason.values()) {
                    if (reason.isCensored(word)) {
                        list.put(cnt, new CensoredWord(word, reason, reason.getCorrect()));
                    }
                }
            }
            cnt++;
        }
        return list;
    }

    public List<String> getWhitelistedWords() {
        List<String> list = (List<String>) mainNode.getCurrentNode().getList("whitelist");
        return list;
    }

}
