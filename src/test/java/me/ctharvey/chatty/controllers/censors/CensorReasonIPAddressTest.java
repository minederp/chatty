/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.chatty.controllers.censors;

import junit.framework.TestCase;

/**
 *
 * @author thronecth
 */
public class CensorReasonIPAddressTest extends TestCase {
    
    public CensorReasonIPAddressTest(String testName) {
        super(testName);
    }

    public void testIsCensored() {
        assert(CensorReason.IPADDRESS.isCensored("192.68.0.1"));
        assert(CensorReason.IPADDRESS.isCensored("192.68.0.1:8080"));
        assert(CensorReason.IPADDRESS.isCensored("http://192.68.0.1"));
        assertFalse(CensorReason.IPADDRESS.isCensored("hi"));
    }

    
}
