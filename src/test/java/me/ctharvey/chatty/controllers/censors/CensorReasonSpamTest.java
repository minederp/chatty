/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.chatty.controllers.censors;

import junit.framework.TestCase;

/**
 *
 * @author thronecth
 */
public class CensorReasonSpamTest extends TestCase {
    
    public CensorReasonSpamTest(String testName) {
        super(testName);
    }

    public void testIsCensored() {
        assert(CensorReason.SPAM.isCensored("AAAAAAAAAAAAAAAAAAAAAAAA"));
        System.out.print(CensorReason.SPAM.getCorrect());
        assert(CensorReason.SPAM.isCensored("00000000000000aaaaaaaa"));
        System.out.print(CensorReason.SPAM.getCorrect());
        assertFalse(CensorReason.SPAM.isCensored("00000000000000"));
        assertFalse(CensorReason.SPAM.isCensored("aabbcc"));
        
    }
    
}
