/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.chatty.controllers.censors;

import junit.framework.TestCase;

/**
 *
 * @author thronecth
 */
public class CensorReasonWebsiteTest extends TestCase {
    
    public CensorReasonWebsiteTest(String testName) {
        super(testName);
    }

    public void testIsCensored() {
        assertFalse(CensorReason.WEBSITE.isCensored("minederp.com"));
        assert(CensorReason.WEBSITE.isCensored("minehacks.net"));
        assertFalse(CensorReason.WEBSITE.isCensored("minecraft.net"));
        assert(CensorReason.WEBSITE.isCensored("minehacks.net"));
        assert(CensorReason.WEBSITE.isCensored("minehacks.net"));
        assert(CensorReason.WEBSITE.isCensored("minehacks.net"));
        assertFalse(CensorReason.WEBSITE.isCensored("minehacks"));
    }
    
}
